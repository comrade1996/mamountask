package com.mamun.backend.domain;

import com.mamun.backend.dto.FormDataDTO;
import com.mamun.backend.helpers.Utils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;

import javax.validation.ValidationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MamounCSV {//refactor to better name TODO

    public static ResponseEntity<ByteArrayResource> filesProcessor(FormDataDTO data)throws IOException, NoSuchMethodException {
        String fileName = Utils.getSaltString();
        var file1List=  Utils.formattedCols(Utils.fileDecoder(data.file1), data.numberOfLines);
        var file2List= Utils.formattedCols(Utils.fileDecoder(data.file1), data.numberOfLines);
        Collections.reverse(file2List);
        if (file1List.size()>1000){
            throw new ValidationException();//refactor TODO
        }
        file1List = CollationPolicyFormatter(data.collationPolicy,file1List);
        file2List =  CollationPolicyFormatter(data.collationPolicy,file2List);
        Utils.FileBuilder("csv",fileName,MamounCSV.CollationPolicyJoiner(data.collationPolicy,file1List,file2List));


        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(Path.of(fileName)));
        Files.deleteIfExists(Path.of(fileName));
        return ResponseEntity.ok()
                .body(resource);
    }

    public static List<String> CollationPolicyFormatter(String collationPolicy, List<String> dataList){
        if (collationPolicy.equals("normal") ){
            dataList.removeIf(String::isEmpty);
        }
        return dataList;
    }

    public static List<String> CollationPolicyJoiner(String collationPolicy,List<String> file1List,List<String> file2List){
        if (collationPolicy.equals("normal") ){
            if (file1List.size()>file2List.size())
                file1List=  file1List.subList(0,file2List.size()-1);

            if (file1List.size()<file2List.size())
                file2List = file2List.subList(0, file1List.size() - 1);
        }

        List<String> dataList = new ArrayList<String>(file1List);
        dataList.addAll(file2List);
        return dataList;
    }

}
