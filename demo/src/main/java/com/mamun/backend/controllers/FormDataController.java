package com.mamun.backend.controllers;

import com.mamun.backend.domain.MamounCSV;
import com.mamun.backend.dto.FormDataDTO;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@CrossOrigin(origins = "*")
@RestController
public class FormDataController {

    @PostMapping("api/form-data")
    public ResponseEntity<ByteArrayResource> Task(@Valid @RequestBody FormDataDTO data) throws IOException, NoSuchMethodException {

        return MamounCSV.filesProcessor(data);
    }

 @GetMapping("test")
    public String testTask(){

        return "test";
    }


    @MessageMapping("api/ws-form-data")
    @SendTo("api/topic/form-data-messages")
    public ResponseEntity<ByteArrayResource> wsTask(@Valid @RequestBody FormDataDTO data) throws IOException, NoSuchMethodException {

       return MamounCSV.filesProcessor(data);
    }



}


