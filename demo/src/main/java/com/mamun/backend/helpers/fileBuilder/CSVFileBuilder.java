package com.mamun.backend.helpers.fileBuilder;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class CSVFileBuilder implements FileBuilder{

    private static CSVFileBuilder INSTANCE;

    private CSVFileBuilder() {
    }

    public static CSVFileBuilder getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new CSVFileBuilder();
        }

        return INSTANCE;
    }

    @Override
    public void fileBuilderFromList(String filename,List<String> dataList) {
        try (PrintWriter writer = new PrintWriter(filename)) {

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <dataList.size() /2; i++) {
                sb.append(dataList.get(i));
                sb.append(',');
                sb.append(dataList.get(dataList.size() /2+ i));
                sb.append('\n');
            }


            writer.write(sb.toString());
            System.out.println("done!");

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
