package com.mamun.backend.helpers;

import com.mamun.backend.helpers.fileBuilder.FileBuilderFactory;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Random;

public class Utils {
 public static String fileDecoder(String fileBase64){
     byte[] decodedBytes = Base64.getDecoder().decode(fileBase64);

     return new String(decodedBytes);
 }
 public static List<String> formattedCols(String formattedFile, int numberOfLines){
     List<String> temp = new ArrayList<>();
     formattedFile.lines().limit(numberOfLines).forEach(row -> {
       temp.add(row.split(",")[0]);
     });
     return temp;
 }

    public static void  FileBuilder(String filetype,String filename,List<String> dataList) {
        FileBuilderFactory factory = new FileBuilderFactory();
         var fileBuilder =  factory.getInstance(filetype);
         fileBuilder.fileBuilderFromList(filename,dataList);

    }
    public static  String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { 
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }
}
