package com.mamun.backend.dto;


import javax.validation.constraints.*;

public class FormDataDTO {

    @NotBlank(message = "Please provide Collation Policy")
    @Pattern(regexp="^[A-Za-z]*$",message = "Invalid Collation Policy")
    public String collationPolicy;
    @NotBlank (message = "Please provide csv file")
    public String file1;

    @NotBlank (message = "Please provide csv file")
    public String file2;
//
//    @Size(min = 5, message = "Number of Lines should be equal or more than 5")
//    @Size(max = 100, message = "Number of Lines should be equal or less than 100")
   @Min(5)
   @Max(100)
    public int numberOfLines;

    public boolean download;


}
