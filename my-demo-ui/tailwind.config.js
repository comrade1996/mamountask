module.exports = {
    prefix: '',
    purge: {
      enabled: process.env.NODE_ENV === 'production',
      content: [
        './src/**/*.{html,ts}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
      extend: {},
    },
    variants: {
        backgroundColor: ({ after }) => after(['disabled']),
      extend: {},
    }
    //plugins: [require('@tailwindcss/forms'),require('@tailwindcss/typography')],
};