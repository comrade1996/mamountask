import { Injectable } from '@angular/core';
import {httpApiService} from "../utils/common/services/httpApiService.service";
import {CSVFormData} from "../contracts/CSVFormData";
import {APIProtocolType} from "../utils/common/constants/App.Constant";
import {Observable} from "rxjs";
import {WebsocketAPIService} from "../utils/common/services/websocket-api.service";

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  constructor(private httpAPIService:httpApiService,private webSocketAPIService:WebsocketAPIService) {

  }

    public SendFormData(data: CSVFormData, Protocol: APIProtocolType):Observable<any>{
      
      switch (Protocol) {
          case APIProtocolType.HTTP:
            return this.httpAPIService.post('form-data',data,{responseType: 'text'});
          case APIProtocolType.WEBSOCKET:
          {
              this.webSocketAPIService.onSendMessage('/app/ws-form-data',data);
              this.webSocketAPIService.onReceiveMessage('/topic/form-data-messages"')
              return this.webSocketAPIService.receivedMessage();
          }
          default:
              return this.httpAPIService.post('form-data', data, {responseType: 'text'});

      }
    }
}
