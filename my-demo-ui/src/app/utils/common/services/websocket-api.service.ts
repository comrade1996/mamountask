import { Injectable } from '@angular/core';
import {Observable, Subject, Subscription} from "rxjs";
import {RxStompService} from "@stomp/ng2-stompjs";

@Injectable({
  providedIn: 'root'
})
export class WebsocketAPIService {
    private receivedMessages$ = new Subject();
    private topicSubscription: Subscription | undefined;

    constructor(private rxStompService: RxStompService) {}

    ngOnInit() {

    }

    ngOnDestroy() {
        // @ts-ignore
        this.topicSubscription.unsubscribe();
    }
    public  receivedMessage():Observable<any>{
        return this.receivedMessages$.asObservable();
    }
    onReceiveMessage(url:string){
        this.topicSubscription = this.rxStompService
            .watch(url)
            .subscribe((message: any) => {
                this.receivedMessages$.next(message.body);
            });
    }
    onSendMessage(url:string,data:any) {

        this.rxStompService.publish({ destination: url, body: data });
    }
}
