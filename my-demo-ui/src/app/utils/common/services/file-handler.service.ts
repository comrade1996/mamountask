import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileHandlerService {

  constructor() {

  }

    formatBase64File(file:string):string{
        const formattedFile = file.split('base64,');

        return formattedFile[1];
    }

    makeCSVFile(text:any) {
      // TODO refactor to Factory pattern to easily extending files type supported
        let data = new Blob([text], {type: 'text/csv'});

        console.log(data)
        return data;
    };
  downloadData(data:any){
      let file = window.URL.createObjectURL(data);
      const link = document.createElement("a");
      link.download= "JoinedFile.csv";
      link.href= file;
      link.click();
  }
}
