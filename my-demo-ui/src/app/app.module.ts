import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {SweetAlert2Module} from "@sweetalert2/ngx-sweetalert2";
import {NgHttpLoaderModule} from "ng-http-loader";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {GlobalHttpErrorInterceptorService} from "./utils/common/errors/GlobalHttpInterceptor";
import {
    InjectableRxStompConfig,
    RxStompService,
    rxStompServiceFactory,
} from '@stomp/ng2-stompjs';
import {myRxStompConfig} from "./utils/common/config/additionalConfigs.configuarations";



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      SweetAlert2Module.forRoot(),
      NgHttpLoaderModule.forRoot(),
      BrowserAnimationsModule,
      ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
      {
          provide: InjectableRxStompConfig,
          useValue: myRxStompConfig,
      },
      {
          provide: RxStompService,
          useFactory: rxStompServiceFactory,
          deps: [InjectableRxStompConfig],
      },

      { provide: HTTP_INTERCEPTORS,    useClass: GlobalHttpErrorInterceptorService,    multi: true  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
