import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {FileHandlerService} from "./utils/common/services/file-handler.service";
import {FormDataService} from "./data/form-data.service";
import {APIProtocolType} from "./utils/common/constants/App.Constant";
import {NotificationService} from "./utils/common/services/notification.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mamoun-SPA';

    // @ts-ignore
    dataForm: FormGroup;
    submitted = false;
    message: any;

    constructor(
        private filesHandler:FileHandlerService,
        private notification:NotificationService,
        private formBuilder: FormBuilder,private  formDataHandler:FormDataService) { }

    ngOnInit() {
        this.dataForm = this.formBuilder.group({
            file1: ['', Validators.required],
            file2: ['', Validators.required],
            numberOfLines: ['', [Validators.required,Validators.min(5), Validators.max(100)]],
            collationPolicy: ['full', Validators.required],
            download: [true, ]
        });
    }
    onFileSelect(event:any,filename:string) {
        let af = ['text/csv']
        if (event.target.files.length > 0) {
            const file = event.target.files[0];

            if (file.type!=af) {
                alert('Only CSV Docs Allowed!');
                // @ts-ignore
                this.dataForm.get(filename).setValue( null);
            } else {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload =  () => {
                    console.log(reader.result)
                    // @ts-ignore
                    this.dataForm.get(filename).setValue(this.filesHandler.formatBase64File(reader.result) );

                };



            }
        }
    }

    get f() { return this.dataForm.controls; }

    onSubmit() {
        this.submitted = true;
          this.formDataHandler.SendFormData(this.dataForm.value,APIProtocolType.HTTP).subscribe((data)=>{

              this.notification.showSuccess("Files proceeded Successfully","Success");// Refactor TODO
           this.f['download'].value?
               this.filesHandler.downloadData(this.filesHandler.makeCSVFile(data)):this.message=data;
        }
        )

    }

    onReset() {
        this.submitted = false;
        this.message=null;
        this.dataForm.reset();
    }
}
