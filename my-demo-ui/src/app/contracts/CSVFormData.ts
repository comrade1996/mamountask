export interface CSVFormData{
    file1:string,
    file2:string,
    numberOfLines:number,
    download:boolean,
    collationPolicy:string
}